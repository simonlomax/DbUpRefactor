﻿using DbUp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using McMaster.Extensions.CommandLineUtils;
using Serilog;

namespace ekmPowershopDbUp
{
    enum MigrationResult
    {
        NotFound = 0
        , Success = 1
        , Failure
    }


    internal class Settings
    {
        public CommandOption Username { get; set; }
        public CommandOption Predeploy { get; set; }
        public CommandOption VerboseLogging { get; set; }
    }

    internal class PowershopData
    {
        public string ServerId { get; }
        public string AccountsDb { get; }
        public string ShopsPath { get; }
        public string EkmpsPath { get; }


        public PowershopData()
        {
            ServerId = getServerId();
            AccountsDb = getAccountsDb();
            ShopsPath = getShopsPath();
            EkmpsPath = getEkmpsPath();
        }

        public IEnumerable<string> GetAccounts(string accountName)
        {
            var sql= $"select username from tbl_accounts where ServerID = {ServerId} and username='{accountName}'";
            return executeSql(sql);
        }

        public IEnumerable<string> GetAccounts()
        {
            var sql = $"select username from tbl_accounts where ServerID = {ServerId} order by username" ;
            return executeSql(sql);
        }

        private string getShopsPath()
        {
            var dataPath = ConfigurationManager.AppSettings["ekm_data"];
            return Path.Combine(dataPath, @"shops\");
        }
        private string getEkmpsPath()
        {
            return ConfigurationManager.AppSettings["ekmps"];
        }

        private string getAccountsDb()
        {
            var dataPath = ConfigurationManager.AppSettings["ekm_data"];
            var connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            var accountsDb = Path.Combine(dataPath, "accounts.sqlite");
            return $"{connectionString}{accountsDb}";
        }

        private string getServerId()
        {
            var serveridIncFile = ConfigurationManager.AppSettings["ServerIdInc"];
            if (!File.Exists(serveridIncFile)) return "";
            return File.ReadAllLines(serveridIncFile)
                       .FirstOrDefault(l => l.Contains("SI_SERVERID"))
                       ?.Split(new[] { '=' }, StringSplitOptions.None)
                       .LastOrDefault()?.Trim().Replace("\"", "")
                   ?? "-1";

        }

        private IEnumerable<string> executeSql(string sql)
        {
            var accounts = new List<string>();
            //if (!File.Exists(AccountsDb)) return accounts;

            try
            {
                using (var conn = new SQLiteConnection(AccountsDb))
                {
                    using (var cmd = new SQLiteCommand(sql, conn))
                    {
                        conn.Open();
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                accounts.Add(reader["Username"].ToString().Trim());
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return accounts;
        }
    }


    class Program
    {

        static readonly List<string> erroringStores = new List<string>();
        private static PowershopData _data;
        private static CommandLineApplication _app;
        private static Settings _settings;

        static int Main(string[] args)
        {
            configureSettings();
            configureLogging();

            _app.OnExecute(() =>
            {
                MigrateData();
#if DEBUG
                Console.WriteLine("Press any key...");
                Console.ReadKey();
#endif
                // Return something appropriate so DBTools knows if upgrade was successful or not 
                return 0;
            });

           
            _app.OnValidationError(v =>
            {
                Console.WriteLine(v.ErrorMessage);
                return 1;
            });

            try
            {
                return _app.Execute(args);
            }
            catch (Exception)
            {
                _app.ShowHelp();
                return 1;
            }
        }

        private static void configureLogging()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                //.WriteTo.File("log-.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }

        private static void configureSettings()
        {
            _data = new PowershopData();

            _app = new CommandLineApplication();
            _app.HelpOption();
            _app.ExtendedHelpText = @"
Usage:
   Running without any command line arguments will cause all shops referenced in the 
   local accounts database to be migrated to the latest version.
            ";

            _settings = new Settings
            {
                Username = _app.Option("-u|--username <Username>", "The username of the shop to migrate", CommandOptionType.SingleValue),
                Predeploy = _app.Option("-p|--predeploy", "Run any predeploy scripts", CommandOptionType.NoValue),
                VerboseLogging = _app.Option("-l|--log", "Turn verbose logging", CommandOptionType.NoValue)
            };
        }


        internal static void MigrateData()
        {
            if (_settings.Username.Value() == null)
            {
                UpdateBlankForeignDatabases();
                UpdateDemoShopDatabases();
            }

            var dbs = string.IsNullOrEmpty(_settings.Username.Value())
                ? _data.GetAccounts()
                : _data.GetAccounts(_settings.Username.Value());

            UpdateShopDatabases(dbs);

        }

        internal static void UpdateDb(IEnumerable<string> dbs, Func<string, bool> predicate)
        {

            foreach (string db in dbs)
            {
                var dbPath = $@"{_data.ShopsPath}{db}.sqlite";

                var result = Upgrader.PerformDbUpgrade(dbPath, predicate);
                var resultText = $"# {db}: [{result}]";
                Log.Information(resultText);
            }
        }


        internal static void UpdateShopDatabases(IEnumerable<string> dbs)
        {
            var predicate = _settings.Predeploy.HasValue()
                ? (Func<string, bool>) (script => (!script.Contains("__SHOPS__") && script.Contains("__BEFORE__")))
                : script => !script.Contains("__SHOPS__");

            UpdateDb(dbs, predicate);
        }

        internal static void UpdateBlankForeignDatabases()
        {
            string[] dbs =
            {
                "ekm_blank",
                "ekm_blank_ireland",
                "ekm_blank_australia",
                "ekm_blank_india",
                "ekm_blank_english_latam",
                "ekm_blank_france",
                "ekm_blank_es",
                "ekm_blank_latam",
                "ekm_blank_argentina",
                "ekm_blank_colombia",
                "ekm_blank_mexico",
                "ekm_blank_peru",
                "ekm_blank_stax",
                "ekm_blank_tiger"
            };

            var dbPaths = dbs.Select(db => $@"{_data.EkmpsPath}\{db}\data\database.sqlite");

            var predicate = _settings.Predeploy.HasValue()
                ? (Func<string, bool>) (script => (!script.Contains("__BLANKS__") && script.Contains("__BEFORE__")))
                : script => !script.Contains("__BLANKS__");

            UpdateDb(dbPaths, predicate);
        }

        internal static void UpdateDemoShopDatabases()
        {

            string[] dbs =
            {
                "demoshop_restore",
                "demoshop_restore_english_latam",
                "demoshop_restore_ireland",
                "demoshop_restore_france",
                "demoshop_restore_es",
                "demoshop_restore_latam",
                "demoshop_restore_argentina",
                "demoshop_restore_colombia",
                "demoshop_restore_mexico",
                "demoshop_restore_peru"
            };

            var dbPaths = dbs.Select(db => $@"{_data.EkmpsPath}\{db}\data\demoshop.sqlite");
            var predicate = _settings.Predeploy.HasValue()
                ? (Func<string, bool>)(script => (!script.Contains("__BLANKS__") && script.Contains("__BEFORE__")))
                : script => !script.Contains("__BLANKS__");

            UpdateDb(dbPaths, predicate);
        }

        public static class Upgrader
        {
            private static readonly string _connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            public static MigrationResult PerformDbUpgrade(string dbPath, Func<string, bool> predicate)
            {

                if (!File.Exists(dbPath)) return MigrationResult.NotFound;
                
                var db = $"{_connectionString}{dbPath}";

                var result = _settings.VerboseLogging.HasValue()
                    ? DeployChanges.To.SQLiteDatabase(db)
                        .WithScriptsAndCodeEmbeddedInAssembly(Assembly.GetExecutingAssembly(), predicate)
                        .LogToAutodetectedLog()
                        .Build()
                        .PerformUpgrade().Successful
                    : DeployChanges.To.SQLiteDatabase(db)
                        .WithScriptsAndCodeEmbeddedInAssembly(Assembly.GetExecutingAssembly(), predicate)
                        .LogToNowhere()
                        .Build()
                        .PerformUpgrade().Successful;

                return result ? MigrationResult.Success : MigrationResult.Failure;
            }
        }
    }
}

