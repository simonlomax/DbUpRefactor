﻿using DbUp.Engine;
using System;
using System.Data;

namespace ekmPowershopDbUp.Scripts
{
    public class S20180614BS1__Switch_on_full_site_ssl : IScript
    {

        public string ProvideScript(Func<IDbCommand> commandFactory)
        {
            var shopHasSecureDomain = false;

            var command = commandFactory();

            command.CommandText = "SELECT ElementContent FROM tbl_Elements WHERE ElementReference='_EKM_SECURE_DOMAIN_NAME';";
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        string ElementContent = reader.GetString(0);

                        if (!string.IsNullOrEmpty(ElementContent))
                        {
                            shopHasSecureDomain = true;
                        }
                    }
                }
            }


            //UPDATE SCRIPT           
            if (shopHasSecureDomain)
            {
                command.CommandText = "INSERT OR REPLACE INTO tbl_Elements (ElementReference,ElementContent,ElementContent_Default,ElementType,Element_DataType,Info_Name,Info_Description) ";
                command.CommandText += "VALUES ('_EKM_FULL_SITE_SSL', 'TRUE', 'TRUE','SYSTEM','','System Setting','General System setting for domains');";
                command.ExecuteNonQuery();
            }

            return "";
        }

    }
}
