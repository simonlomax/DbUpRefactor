INSERT INTO tbl_SystemOptions (`OptionCode`, `OptionName`, `MenuActive`, `OptionDescription`, `Url`)
SELECT 'INTEGRATION-REVIEWSIO', 'Reviews.io', 1, 'Reviews.io', 'https://www.reviews.io/'
WHERE NOT EXISTS(SELECT 1 FROM tbl_SystemOptions WHERE OptionCode='INTEGRATION-REVIEWSIO');
