﻿CREATE INDEX IF NOT EXISTS 'FeedCategoryMappings_destination_category_id' ON [FeedCategoryMappings]([destination_category_id] ASC);

CREATE INDEX IF NOT EXISTS 'FeedCategoryMappings_source_category_path' ON [FeedCategoryMappings]([source_category_path] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_BlockVisitors_Block' ON [tbl_BlockVisitors]([Block] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_BlockVisitors_Type' ON [tbl_BlockVisitors]([Type] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CountryCodes_AllowCurrency' ON [tbl_CountryCodes]([AllowCurrency] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CountryCodes_AlphaCode' ON [tbl_CountryCodes]([AlphaCode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CountryCodes_CountryCode' ON [tbl_CountryCodes]([CountryCode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CountryCodes_NumericCode' ON [tbl_CountryCodes]([NumericCode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CountryCodes_SymbolHTMLCode' ON [tbl_CountryCodes]([SymbolHTMLCode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CountryCodes_id' ON [tbl_CountryCodes]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CustomFields_FieldOrder' ON [tbl_CustomFields]([FieldOrder] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CustomFields_FieldType' ON [tbl_CustomFields]([FieldType] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CustomFields_id' ON [tbl_CustomFields]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CustomerReviews_ProductID' ON [tbl_CustomerReviews]([ProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CustomerReviews_Review_Approved' ON [tbl_CustomerReviews]([Review_Approved] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_CustomerReviews_id' ON [tbl_CustomerReviews]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_Customers_Notes_CustomerID' ON [tbl_Customers_Notes]([CustomerID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_Customers_Notes_NoteType' ON [tbl_Customers_Notes]([NoteType] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_Customers_Notes_user' ON [tbl_Customers_Notes]([user] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DeliveryPriceMatrix_DeliveryID' ON [tbl_DeliveryPriceMatrix]([DeliveryID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DeliveryPriceMatrix_PriceFrom' ON [tbl_DeliveryPriceMatrix]([PriceFrom] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DeliveryPriceMatrix_PriceTo' ON [tbl_DeliveryPriceMatrix]([PriceTo] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DeliveryWeightMatrix_WeightFrom' ON [tbl_DeliveryWeightMatrix]([WeightFrom] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DeliveryWeightMatrix_WeightTo' ON [tbl_DeliveryWeightMatrix]([WeightTo] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DiscountSystem_BulkDiscount_ProductID' ON [tbl_DiscountSystem_BulkDiscount]([ProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DiscountSystem_BulkDiscount_RangeFrom' ON [tbl_DiscountSystem_BulkDiscount]([RangeFrom] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DiscountSystem_BulkDiscount_RangeTo' ON [tbl_DiscountSystem_BulkDiscount]([RangeTo] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DiscountSystem_BulkDiscount_id' ON [tbl_DiscountSystem_BulkDiscount]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DiscountSystem_CouponCategories_id' ON [tbl_DiscountSystem_CouponCategories]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DiscountSystem_Coupons_Coupon_Code' ON [tbl_DiscountSystem_Coupons]([Coupon_Code] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DiscountSystem_Coupons_id' ON [tbl_DiscountSystem_Coupons]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_DiscountSystem_id' ON [tbl_DiscountSystem]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ElementImages_ElementReference' ON [tbl_ElementImages]([ElementReference] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ElementImages_SlideNumber' ON [tbl_ElementImages]([SlideNumber] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_EmailBackInStock_Email' ON [tbl_EmailBackInStock]([Email] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_EmailBackInStock_ProductID' ON [tbl_EmailBackInStock]([ProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_Files_FileLocation' ON [tbl_Files]([FileLocation] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_Files_FileName' ON [tbl_Files]([FileName] ASC);

CREATE UNIQUE INDEX IF NOT EXISTS 'tbl_FilterableByBrands_category_id' ON [tbl_FilterableByBrands]([category_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_FilterableByBrands_is_filterable' ON [tbl_FilterableByBrands]([is_filterable] ASC);

CREATE UNIQUE INDEX IF NOT EXISTS 'tbl_FilterableByPrices_category_id' ON [tbl_FilterableByPrices]([category_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_FilterableByPrices_is_filterable' ON [tbl_FilterableByPrices]([is_filterable] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_FilterableProductAttributes_attribute_key' ON [tbl_FilterableProductAttributes]([attribute_key] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_FilterableProductAttributes_attribute_key_category_id' ON [tbl_FilterableProductAttributes]([attribute_key] ASC,[category_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_FilterableProductAttributes_category_id' ON [tbl_FilterableProductAttributes]([category_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_PaymentGateways_CodeName' ON [tbl_PaymentGateways]([CodeName] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_PaymentGateways_NumberDetails_1' ON [tbl_PaymentGateways]([NumberDetails_1] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_PaymentGateways_NumberDetails_2' ON [tbl_PaymentGateways]([NumberDetails_2] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_PaymentGateways_NumberDetails_3' ON [tbl_PaymentGateways]([NumberDetails_3] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_PaymentGateways_NumberDetails_4' ON [tbl_PaymentGateways]([NumberDetails_4] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_PaymentGateways_id' ON [tbl_PaymentGateways]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductAttributesItems_AttributeKey' ON [tbl_ProductAttributesItems]([AttributeKey] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductAttributesItems_AttributeValue' ON [tbl_ProductAttributesItems]([AttributeValue] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductAttributesItems_OrderLocation' ON [tbl_ProductAttributesItems]([OrderLocation] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductAttributesItems_ProductID' ON [tbl_ProductAttributesItems]([ProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductDeliveryCharges_DeliveryID' ON [tbl_ProductDeliveryCharges]([DeliveryID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductDeliveryCharges_ProductID' ON [tbl_ProductDeliveryCharges]([ProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductOptionsItems_OptionImage' ON [tbl_ProductOptionsItems]([OptionImage] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductOptionsItems_ProductOptionsID' ON [tbl_ProductOptionsItems]([ProductOptionsID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductOptionsItems_id' ON [tbl_ProductOptionsItems]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductOptions_OptionType' ON [tbl_ProductOptions]([OptionType] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductOptions_ProductID' ON [tbl_ProductOptions]([ProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductOptions_id' ON [tbl_ProductOptions]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ProductRelationships_ProductID' ON [tbl_ProductRelationships]([ProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_Redirects_FromURL' ON [tbl_Redirects]([FromURL] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_Redirects_ToURL' ON [tbl_Redirects]([ToURL] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_SearchStatistics_SearchTerm' ON [tbl_SearchStatistics]([SearchTerm] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_SessionData_created' ON [tbl_SessionData]([created] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_SessionData_expires' ON [tbl_SessionData]([expires] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_SessionData_session_id' ON [tbl_SessionData]([session_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_SystemOptions_id' ON [tbl_SystemOptions]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_WebPages_Active' ON [tbl_WebPages]([Active] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_WebPages_GroupBy' ON [tbl_WebPages]([GroupBy] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_WebPages_Groups_Active' ON [tbl_WebPages_Groups]([Active] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_WebPages_OrderBy' ON [tbl_WebPages]([OrderBy] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_WebPages_PageIdentifier' ON [tbl_WebPages]([PageIdentifier] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessionsItems_GUID' ON [tbl_cartSessionsItems]([GUID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessionsItems_ProductDescriptionHash' ON [tbl_cartSessionsItems]([ProductDescriptionHash] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessionsItems_ProductID' ON [tbl_cartSessionsItems]([ProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessionsItems_id' ON [tbl_cartSessionsItems]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessionsItems_uID' ON [tbl_cartSessionsItems]([uID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessions_CountryCode' ON [tbl_cartSessions]([CountryCode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessions_CurrencyCode' ON [tbl_cartSessions]([CurrencyCode] ASC);

CREATE UNIQUE INDEX IF NOT EXISTS 'tbl_cartSessions_GUID' ON [tbl_cartSessions]([GUID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessions_OrderNumber' ON [tbl_cartSessions]([OrderNumber] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessions_id' ON [tbl_cartSessions]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_cartSessions_uID' ON [tbl_cartSessions]([uID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_categories_Status' ON [tbl_categories]([Status] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_categories_id' ON [tbl_categories]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_categories_image' ON [tbl_categories]([image] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_categories_orderlocation' ON [tbl_categories]([orderlocation] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_categories_parent_cat' ON [tbl_categories]([parent_cat] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_categories_sub_cat' ON [tbl_categories]([sub_cat] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_categories_unique_identifier' ON [tbl_categories]([unique_identifier] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_classifications_class_ItemID' ON [tbl_classifications]([class_ItemID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_classifications_id' ON [tbl_classifications]([id] ASC);

CREATE UNIQUE INDEX IF NOT EXISTS 'tbl_customers_GUID' ON [tbl_customers]([GUID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_customers_email' ON [tbl_customers]([email] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_customers_id' ON [tbl_customers]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_customers_password' ON [tbl_customers]([password] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_customers_postcode' ON [tbl_customers]([postcode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_design_id' ON [tbl_design]([id] ASC);

CREATE UNIQUE INDEX IF NOT EXISTS 'tbl_design_tag_guid' ON [tbl_design_tag]([guid] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ekmProducts_Code' ON [tbl_ekmProducts]([Code] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ekmProducts_NumberOption1' ON [tbl_ekmProducts]([NumberOption1] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ekmProducts_NumberOption2' ON [tbl_ekmProducts]([NumberOption2] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ekmProducts_NumberOption3' ON [tbl_ekmProducts]([NumberOption3] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ekmProducts_NumberOption4' ON [tbl_ekmProducts]([NumberOption4] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ekmProducts_NumberOption5' ON [tbl_ekmProducts]([NumberOption5] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_ekmProducts_id' ON [tbl_ekmProducts]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_general_CountryCode' ON [tbl_general]([CountryCode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_images_filename' ON [tbl_images]([filename] ASC);

CREATE UNIQUE INDEX IF NOT EXISTS 'tbl_images_id' ON [tbl_images]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_images_location' ON [tbl_images]([location] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_images_unique_identifier' ON [tbl_images]([unique_identifier] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orderitems_id' ON [tbl_orderitems]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orderitems_item_id' ON [tbl_orderitems]([item_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orderitems_order_number' ON [tbl_orderitems]([order_number] ASC);

CREATE UNIQUE INDEX IF NOT EXISTS 'tbl_orders_GUID' ON [tbl_orders]([GUID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_PageProgression' ON [tbl_orders]([PageProgression] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_TransactionExpiryDate' ON [tbl_orders]([TransactionExpiryDate] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_cc_securitycode' ON [tbl_orders]([cc_securitycode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_customerid' ON [tbl_orders]([customerid] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_id' ON [tbl_orders]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_order_number' ON [tbl_orders]([order_number] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_orderdate' ON [tbl_orders]([orderdate] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_shipping_postcode' ON [tbl_orders]([shipping_postcode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_status' ON [tbl_orders]([status] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_orders_transid' ON [tbl_orders]([transid] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_Brand' ON [tbl_products]([Brand] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_Feed' ON [tbl_products]([Feed] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_MultipleCategory_MasterProductID' ON [tbl_products]([MultipleCategory_MasterProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_NumberInStock' ON [tbl_products]([NumberInStock] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_ProductCode' ON [tbl_products]([ProductCode] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_Status' ON [tbl_products]([Status] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_TotalProductStock' ON [tbl_products] ([TotalProductStock]);

CREATE INDEX IF NOT EXISTS 'tbl_products_cat_id' ON [tbl_products]([cat_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_delivery_charge' ON [tbl_products]([delivery_charge] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_filterby_brand' ON [tbl_products]([sub_cat_marker] ASC,[parentproduct_id] ASC,[Status] ASC,[Brand] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_filterby_category' ON [tbl_products]([cat_id] ASC,[sub_cat_marker] ASC,[Status] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_filterby_category_managed_products' ON [tbl_products]([cat_id] ASC,[sub_cat_marker] ASC,[Status] ASC,[MultipleCategory_MasterProductID] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_id' ON [tbl_products]([id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_image' ON [tbl_products]([image] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_image2' ON [tbl_products]([image2] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_image3' ON [tbl_products]([image3] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_image4' ON [tbl_products]([image4] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_image5' ON [tbl_products]([image5] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_orderlocation' ON [tbl_products]([orderlocation] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_parentproduct_id' ON [tbl_products]([parentproduct_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_special_offer' ON [tbl_products]([special_offer] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_sub_cat_id' ON [tbl_products]([sub_cat_id] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_sub_cat_marker' ON [tbl_products]([sub_cat_marker] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_tax_applicable' ON [tbl_products]([tax_applicable] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_unique_identifier' ON [tbl_products]([unique_identifier] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_variant_description' ON [tbl_products]([parentproduct_id] ASC,[description] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_products_variant_price' ON [tbl_products]([parentproduct_id] ASC,[price] ASC);

CREATE INDEX IF NOT EXISTS 'tbl_tempCartSessions_GUID' ON [tbl_tempCartSessions]([GUID] ASC);
