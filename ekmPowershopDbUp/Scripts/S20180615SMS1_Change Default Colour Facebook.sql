CREATE TEMP TABLE tbl_tempFacebookMessenger AS SELECT * FROM tbl_FacebookMessenger;

DROP TABLE tbl_FacebookMessenger;

CREATE TABLE tbl_FacebookMessenger (
	Id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	PageId	TEXT NOT NULL UNIQUE COLLATE NOCASE,
	ThemeColour	TEXT NOT NULL DEFAULT '#FFFFFF' COLLATE NOCASE,
	LoggedInMessage	TEXT DEFAULT '' COLLATE NOCASE,
	LoggedOutMessage TEXT DEFAULT '' COLLATE NOCASE,
	GreetingDialogDisplay TEXT DEFAULT 'show' COLLATE NOCASE,
	GreetingDialogDelay	INTEGER DEFAULT 3
);

INSERT INTO tbl_FacebookMessenger SELECT * FROM tbl_tempFacebookMessenger;

DROP TABLE tbl_tempFacebookMessenger;