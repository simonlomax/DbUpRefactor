﻿using System;
using System.Data;
using DbUp.Engine;

namespace ekmPowershopDbUp.Scripts
{
    class S20180425PS1_CategoryLinkingTableAndTrigger : IScript
    {
        public string ProvideScript(Func<IDbCommand> dbCommandFactory)
        {
            var command = dbCommandFactory();

            //Create table
            command.CommandText = createTableSql();
            command.ExecuteNonQuery();

            //Data exists
            bool exists = false;
            command.CommandText = "SELECT Count(*) 'Count' FROM tbl_CategoryLinking";
            using (var reader = command.ExecuteReader())
            {
                int nameIndex = reader.GetOrdinal("Count");
                while (reader.Read())
                {
                    if (reader.GetInt32(nameIndex) > 0) //CHECK FOR EXISTING DATA
                    {
                        exists = true;
                    }
                }
            }

            if (!exists)
            {
                //Insert for existing records
                command.CommandText = updateForExistingRecords();
                command.ExecuteNonQuery();
            }

            //Insert Trigger
            command.CommandText = insertTrigger();
            command.ExecuteNonQuery();

            //Update Trigger
            command.CommandText = updateTrigger();
            command.ExecuteNonQuery();

            //Delete Trigger
            command.CommandText = deleteTrigger();
            command.ExecuteNonQuery();

            return "";
        }

        private string createTableSql()
        {
            return @"CREATE TABLE IF NOT EXISTS `tbl_CategoryLinking` (
	`Id`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	`ParentCategory_Id`	INTEGER,
	`ChildCategory_Id`	INTEGER,
	`OrderLocation`	INTEGER DEFAULT 99999,
	`Visible`	BOOLEAN NOT NULL DEFAULT 1,
	FOREIGN KEY(`ParentCategory_Id`) REFERENCES `tbl_categories`(`id`),
	FOREIGN KEY(`ChildCategory_Id`) REFERENCES `tbl_categories`(`id`)
);";
        }

        private string insertTrigger()
        {
            return @"CREATE TRIGGER IF NOT EXISTS insert_category_linking
	AFTER INSERT ON tbl_products WHEN New.`sub_cat_marker` = 1
		BEGIN 
			INSERT INTO tbl_CategoryLinking (`ParentCategory_Id`,`ChildCategory_Id`,`OrderLocation`,`Visible`)
			VALUES (NEW.`cat_id`,NEW.`sub_cat_id`,New.`orderlocation`,New.`Status`);			
		END";
        }

        private string updateTrigger()
        {
            return @"CREATE TRIGGER IF NOT EXISTS update_category_linking
	AFTER UPDATE ON tbl_products WHEN OLD.`sub_cat_marker` = 1
		BEGIN 
			UPDATE tbl_CategoryLinking
			SET `OrderLocation` = New.`orderlocation`,`Visible` = New.`Status`
			WHERE `ParentCategory_Id` = OLD.`cat_id` and `ChildCategory_Id` = OLD.`sub_cat_id`;			
		END";
        }

        private string deleteTrigger()
        {
            return @"CREATE TRIGGER IF NOT EXISTS delete_category_linking
	AFTER DELETE ON tbl_products WHEN OLD.`sub_cat_marker` = 1
		BEGIN 
			DELETE FROM tbl_CategoryLinking
			WHERE `ParentCategory_Id` = OLD.`cat_id` and `ChildCategory_Id` = OLD.`sub_cat_id`;			
		END";
        }

        private string updateForExistingRecords()
        {
            return @"INSERT INTO tbl_CategoryLinking (`ParentCategory_Id`,`ChildCategory_Id`,`OrderLocation`,`Visible`)
SELECT `cat_id`, `sub_cat_id`, `orderlocation`,`Status`
FROM tbl_products
WHERE sub_cat_marker = 1;";
        }
    }
}
