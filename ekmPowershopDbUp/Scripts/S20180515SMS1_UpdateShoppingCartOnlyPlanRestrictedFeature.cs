using System;
using System.Data;
using DbUp.Engine;

namespace ekmPowershopDbUp.Scripts
{
    class S20180515SMS1_UpdateShoppingCartOnlyPlanRestrictedFeature : IScript
    {
        public string ProvideScript(Func<IDbCommand> dbCommandFactory)
        {
            var selectShoppingOnlyCartCommand = dbCommandFactory();
            var selectShoppingCartOnlyCommand = dbCommandFactory();

            selectShoppingOnlyCartCommand.CommandText =
                "SELECT id FROM tbl_PlanRestrictedFeatures WHERE id = 'Settings-ShoppingOnlyCart'";

            selectShoppingCartOnlyCommand.CommandText =
                "SELECT id FROM tbl_PlanRestrictedFeatures WHERE id = 'Settings-ShoppingCartOnly'";

            var shoppingOnlyCartResult = Convert.ToString(selectShoppingOnlyCartCommand.ExecuteScalar());
            var shoppingCartOnlyResult = Convert.ToString(selectShoppingCartOnlyCommand.ExecuteScalar());

            if (string.IsNullOrWhiteSpace(shoppingOnlyCartResult) && string.IsNullOrWhiteSpace(shoppingCartOnlyResult))
            {
                return ExecuteInsert(dbCommandFactory);
            }

            if (!string.IsNullOrWhiteSpace(shoppingOnlyCartResult) && !string.IsNullOrWhiteSpace(shoppingCartOnlyResult))
            {
                return ExecuteDelete(dbCommandFactory);
            }

            return string.IsNullOrWhiteSpace(shoppingCartOnlyResult) ? ExecuteUpdate(dbCommandFactory) : string.Empty;
        }

        private static string ExecuteDelete(Func<IDbCommand> dbCommandFactory)
        {
            var deleteCommand = dbCommandFactory();
            deleteCommand.CommandText = "DELETE FROM tbl_PlanRestrictedFeatures WHERE id='Settings-ShoppingOnlyCart'";
            deleteCommand.ExecuteNonQuery();
            return string.Empty;
        }

        private static string ExecuteUpdate(Func<IDbCommand> dbCommandFactory)
        {
            var updateCommand = dbCommandFactory();
            updateCommand.CommandText =
                "UPDATE tbl_PlanRestrictedFeatures SET id = 'Settings-ShoppingCartOnly', minimumPlan = 'LEGACY-BUNDLE' WHERE id = 'Settings-ShoppingOnlyCart';";
            updateCommand.ExecuteNonQuery();
            return string.Empty;
        }

        private static string ExecuteInsert(Func<IDbCommand> dbCommandFactory)
        {
            var insertCommand = dbCommandFactory();
            insertCommand.CommandText =
                "INSERT INTO tbl_PlanRestrictedFeatures (id, minimumPlan, overridePlanRestriction) VALUES ('Settings-ShoppingCartOnly', 'LEGACY-BUNDLE', 0);";
            insertCommand.ExecuteNonQuery();
            return string.Empty;
        }
    }
}