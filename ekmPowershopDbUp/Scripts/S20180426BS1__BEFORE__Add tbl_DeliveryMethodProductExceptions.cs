﻿using DbUp.Engine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ekmPowershopDbUp.Scripts
{
    public class S20180426BS1__BEFORE__Add_tbl_DeliveryMethodProductExceptions : IScript
    {
        public string ProvideScript(Func<IDbCommand> commandFactory)
        {
            var command = commandFactory();

            //COLUMN EXISTS?
            bool columnExists = false;
            string tableName = "tbl_DeliveryMethods";
            string columnName = "EnabledForAllProducts";

            command.CommandText = "PRAGMA TABLE_INFO(" + tableName + ")";

            using (var reader = command.ExecuteReader())
            {
                int nameIndex = reader.GetOrdinal("Name");
                while (reader.Read())
                {
                    columnExists = reader.GetString(nameIndex).Equals(columnName) ? true : false;
                }
            }

            //UPDATE SCRIPT    
            if (!columnExists)
            {
                command.CommandText = "ALTER TABLE tbl_DeliveryMethods ADD COLUMN EnabledForAllProducts BOOLEAN NOT NULL DEFAULT 1";
                command.ExecuteNonQuery();
            }

            //MIGRATE TO NEW TABLE
            var createTable = commandFactory();
            createTable.CommandText = @"CREATE TABLE IF NOT EXISTS [tbl_DeliveryMethodProductExceptions](
                                 [id] INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                                 [DeliveryMethodId] INTEGER NOT NULL,
                                 [ProductId] INTEGER NOT NULL
                                   );";
            createTable.ExecuteNonQuery();

			// Construct list of things that needs inserting
            command.CommandText = "SELECT id, ProductRestriction FROM tbl_DeliveryMethods";
			var dict = new List<KeyValuePair<int, string>>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("ProductRestriction")))
                    {
                        var productIdsStr = reader.GetString(1);
                        var productIds = productIdsStr.Split(new[] { "][" }, StringSplitOptions.None).ToList();

                        if (productIds.Any())
                        {
							var deliveryMethodId = reader.GetInt32(0);
                            dict.AddRange(productIds.Select(x => new KeyValuePair<int, string>(deliveryMethodId, x.Replace("[", "").Replace("]", ""))).ToList());
                        }
                    }
				}
			}
			
			// No truncate in sqlite
			command.CommandText = "DELETE FROM tbl_DeliveryMethodProductExceptions";
			command.ExecuteNonQuery();

			// Clear Autoindex
			command.CommandText = "delete from sqlite_sequence where name='tbl_DeliveryMethodProductExceptions';";
			command.ExecuteNonQuery();
			
			// Reinsert
            var i = 1;
			foreach(var productException in dict) {
                if (productException.Value != string.Empty)
                {
                    command.CommandText = "INSERT INTO tbl_DeliveryMethodProductExceptions (DeliveryMethodId, ProductId) VALUES (" + productException.Key + "," + productException.Value + ")";
                    command.ExecuteNonQuery();
                }
			}
			
            return "";
        }
    }
}
