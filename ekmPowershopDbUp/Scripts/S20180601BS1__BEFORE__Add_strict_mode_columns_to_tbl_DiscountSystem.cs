﻿using DbUp.Engine;
using System;
using System.Data;

namespace ekmPowershopDbUp.Scripts
{
    public class S20180601BS1__BEFORE__Add_strict_mode_columns_to_tbl_DiscountSystem : IScript
    {

        public string ProvideScript(Func<IDbCommand> commandFactory)
        {
            var command = commandFactory();

            //COLUMN EXISTS?
            bool column1Exists = false;
            bool column2Exists = false;
            string tableName = "tbl_DiscountSystem";
            string column1Name = "Trigger_Strict";
            string column2Name = "Reward_Strict";

            command.CommandText = "PRAGMA TABLE_INFO(" + tableName + ")";

            using (var reader = command.ExecuteReader())
            {
                int nameIndex = reader.GetOrdinal("Name");
                while (reader.Read())
                {
                    if (reader.GetString(nameIndex).Equals(column1Name)) //CHECK FOR COLUMN NAME
                    {
                        column1Exists = true;
                    }

                    if (reader.GetString(nameIndex).Equals(column2Name)) //CHECK FOR COLUMN NAME
                    {
                        column2Exists = true;
                    }
                }
            }

            //UPDATE SCRIPT           
            if (!column1Exists)
            {
                command.CommandText = "ALTER TABLE tbl_DiscountSystem ADD COLUMN Trigger_Strict BOOLEAN NOT NULL DEFAULT 0;";
                command.ExecuteNonQuery();
            }

            if (!column2Exists)
            {
                command.CommandText = "ALTER TABLE tbl_DiscountSystem ADD COLUMN Reward_Strict BOOLEAN NOT NULL DEFAULT 0;";
                command.ExecuteNonQuery();
            }

            return "";
        }

    }
}
