BEGIN TRANSACTION;

UPDATE tbl_SystemOptions
SET Url = "/admin/features/#/email-marketing"
WHERE OptionCode = "EKMRESPONSE";

COMMIT;