UPDATE tbl_Elements
SET ElementContent = '[SUBJECT][ekm:shopname] - [ekm:product_name] is now back in stock![/SUBJECT]
                We are pleased to inform you that [ekm:product_name], is now back in stock and available for you to purchase. To view the product please click on the following link [ekm:product_link].<br/>
                If you have any questions, send an email to [ekm:shop_email] or simply reply to this message.<br/>
                [ekm:shopname]<br/>
                [ekm:shop_url]'
WHERE ElementReference = '_EKM_EMAILTEMPLATE_EXTRA_BACKINSTOCKEMAIL' AND ElementContent = '[SUBJECT][ekm:shopname] - [ekm:product_name] is now back in stock![/SUBJECT]';