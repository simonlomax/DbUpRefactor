INSERT OR IGNORE INTO tbl_LanguageElements
(ElementReference, ElementContent)
VALUES 
('LE_GATEWAY_CARD_POWERED_BY_BRAINTREE', 'Braintree');

INSERT OR IGNORE INTO tbl_LanguageElements
(ElementReference, ElementContent)
VALUES 
('LE_GATEWAY_PAYPAL_POWERED_BY_BRAINTREE', 'PayPal');