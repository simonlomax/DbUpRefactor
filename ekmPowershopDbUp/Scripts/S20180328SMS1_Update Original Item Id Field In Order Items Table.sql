BEGIN TRANSACTION;

UPDATE tbl_orderitems SET original_item_id = item_id WHERE item_id > 0;

CREATE INDEX IF NOT EXISTS  OrderItemsOriginalItemId ON tbl_orderitems (
	original_item_id
);

CREATE TRIGGER IF NOT EXISTS insert_original_item_id
AFTER INSERT ON tbl_orderitems 
BEGIN 
    Update tbl_orderitems 
    SET original_item_id = NEW.item_id
    WHERE tbl_orderitems.id = NEW.id;
END;

COMMIT;