﻿CREATE TABLE IF NOT EXISTS tbl_ElementCarouselSettings (
				ElementReference TEXT NOT NULL PRIMARY KEY,
				Animation TEXT,
				AutoRotate BOOLEAN,
				MobileUseDesktopSlides BOOLEAN
				)