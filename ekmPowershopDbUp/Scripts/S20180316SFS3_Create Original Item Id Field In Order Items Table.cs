using DbUp.Engine;
using System;
using System.Data;

namespace ekmPowershopDbUp.Scripts
{
    public class S20180316SFS3_OrderItemsAddColumnOriginalItemId : IScript
    {
        public string ProvideScript(Func<IDbCommand> commandFactory)
        {
            var command = commandFactory();

            //COLUMN EXISTS?
            bool exists = false;
            string tableName = "tbl_orderitems";
            string columnName = "original_item_id";

            command.CommandText = "PRAGMA TABLE_INFO("+ tableName +")";

            using (var reader = command.ExecuteReader())
            {
                int nameIndex = reader.GetOrdinal("Name");
                while (reader.Read())
                {
                    if (reader.GetString(nameIndex).Equals(columnName)) //CHECK FOR COLUMN NAME
                    {
                        exists = true;
                    }
                }
            }

            //UPDATE SCRIPT           
            if (!exists)
            {
                command.CommandText = "ALTER TABLE tbl_orderitems ADD COLUMN original_item_id INTEGER;";
                command.ExecuteNonQuery();
            }

            return "";
        }
    }
}


