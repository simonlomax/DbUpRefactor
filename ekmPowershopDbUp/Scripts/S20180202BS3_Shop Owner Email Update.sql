UPDATE tbl_paymentprocessing SET 
ShopOwnerEmail = replace(ShopOwnerEmail, "To see this order sign in to your ekmPowershop account at http://www.ekmpowershop.com", "To see this order sign in to your EKM account at https://www.ekm.com"),  
ShopOwnerEmail_subject = replace(ShopOwnerEmail_subject, "ekmPowershop: There has been an order at your shop -", "EKM: There has been an order at your shop -");