﻿using DbUp.Engine;
using System;
using System.Collections.Generic;
using System.Data;

namespace ekmPowershopDbUp.Scripts
{
    class S20180626SFS1_Remove__tbl_files_duplicates : IScript
    {
        public string ProvideScript(Func<IDbCommand> dbCommandFactory)
        {
            var dups = GetDups(dbCommandFactory);
            foreach (var dup in dups)
            {
                int id = dup.Item1;
                string name = dup.Item2;
                string location = dup.Item3;

                var command = dbCommandFactory();

                command.CommandText = String.Format(@"
                    DELETE FROM tbl_Files 
                    WHERE Filename='{1}' 
                    AND filelocation='{2}' 
                    AND id <>  {0}
                ", id, name, location);

                command.ExecuteNonQuery();
            }

            return "";
        }

        private List<Tuple<int, string, string>> GetDups(Func<IDbCommand> dbCommandFactory)
        {
            List<Tuple<int, string, string>> list = new List<Tuple<int, string, string>>();

            var command = dbCommandFactory();

            command.CommandText = @"
                SELECT count(filename), Max(id) as maxid, * from tbl_files 
                GROUP BY filename, FileLocation 
                HAVING count(filename) > 1 
            ";

            using (var reader = command.ExecuteReader())
            {
                int idIndex = reader.GetOrdinal("maxid");
                int nameIndex = reader.GetOrdinal("FileName");
                int locationIndex = reader.GetOrdinal("FileLocation");

                while (reader.Read())
                {
                    list.Add(new Tuple<int, string, string>(
                        reader.GetInt32(idIndex),
                        reader.GetString(nameIndex),
                        reader.GetString(locationIndex)
                    ));
                }
            }

            return list;
        }
    }
}
