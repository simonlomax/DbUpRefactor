CREATE TABLE IF NOT EXISTS [tbl_CustomerUsers] 
(
	[Id] INTEGER NOT NULL, 
	[Guid] TEXT(255), 
	[CreatedDate] DATETIME DEFAULT CURRENT_TIMESTAMP, 
	[ModifiedDate] DATETIME DEFAULT CURRENT_TIMESTAMP, 
	[FirstName] TEXT(255), 
	[LastName] TEXT(255), 
	[Email] TEXT(255), 
	[Password] TEXT(255), 
	[PasswordSalt] TEXT(128), 
	[PasswordResetToken] TEXT(128), 
	[PasswordResetTokenDate] DATETIME DEFAULT Null, 
	[AuthTokenSecret] TEXT(128), 
	[Locked] BOOLEAN DEFAULT 0, 
	[LockType] INT DEFAULT 0, 
	[LockedDate] DATETIME DEFAULT Null, 
	[LockedEndDate] DATETIME DEFAULT Null, 
	[LockedReason] TEXT(255), 
	[LoginAttempts] INT, 
	[LastLoginAttemptDate] DATETIME DEFAULT Null, 
	[LastLoginDate] DATETIME DEFAULT Null, 
	[TimesOrdered] INT DEFAULT 0, 
	[LoyaltyPoints] double DEFAULT 0, 
	[Newsletter] double DEFAULT 0,
	[PasswordProtectionLegacyUsername] TEXT(255),  
	PRIMARY KEY(Id)
);

CREATE INDEX IF NOT EXISTS [tbl_CustomerUsers_Guid] 
ON [tbl_CustomerUsers] ([Guid]);

CREATE INDEX IF NOT EXISTS [tbl_CustomerUsers_Email] 
ON [tbl_CustomerUsers] ([Email]);

CREATE INDEX IF NOT EXISTS [tbl_CustomerUsers_PasswordResetToken] 
ON [tbl_CustomerUsers] ([PasswordResetToken]);

CREATE INDEX IF NOT EXISTS [tbl_CustomerUsers_PasswordProtectionLegacyUsername] 
ON [tbl_CustomerUsers] ([PasswordProtectionLegacyUsername]);
