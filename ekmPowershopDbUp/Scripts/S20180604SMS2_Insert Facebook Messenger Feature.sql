INSERT INTO tbl_SystemOptions (`OptionCode`, `OptionName`, `MenuActive`, `OptionDescription`, `Url`, `Installable`)
SELECT 'FEATURE-FACEBOOK-MESSENGER', 'Facebook Messenger', 1, 'Facebook Messenger', 'https://www.messenger.com/', 1
WHERE NOT EXISTS(SELECT 1 FROM tbl_SystemOptions WHERE OptionCode='FEATURE-FACEBOOK-MESSENGER');
