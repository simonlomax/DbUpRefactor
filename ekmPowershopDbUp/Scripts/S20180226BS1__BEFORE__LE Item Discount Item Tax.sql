
INSERT OR IGNORE INTO tbl_LanguageElements (
	ElementReference, 
	ElementContent
) 
VALUES (
	'LE_ITEMDISCOUNT',
	'Item Discount'
);
INSERT OR IGNORE INTO tbl_LanguageElements (
	ElementReference, 
	ElementContent
) 
VALUES (
	'LE_ITEMTAX',
	'Item Tax'
);