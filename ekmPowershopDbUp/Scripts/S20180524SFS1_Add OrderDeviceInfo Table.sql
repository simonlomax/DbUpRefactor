﻿CREATE TABLE IF NOT EXISTS [tbl_OrderDeviceInfo] ( 
	[Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, 
	[OrderNumber] TEXT NOT NULL UNIQUE COLLATE NOCASE, 
	[Completed] BOOLEAN NOT NULL DEFAULT 0, 
	[StartUserAgent] TEXT NULL COLLATE NOCASE, 
	[EndUserAgent] TEXT NULL  COLLATE NOCASE, 
	[StartDevice] TEXT NULL COLLATE NOCASE, 
	[EndDevice] TEXT NULL COLLATE NOCASE 
);

CREATE INDEX IF NOT EXISTS 'tbl_OrderDeviceInfo_OrderNumber' ON [tbl_OrderDeviceInfo]([OrderNumber] ASC);
CREATE INDEX IF NOT EXISTS 'tbl_OrderDeviceInfo_Completed' ON [tbl_OrderDeviceInfo]([Completed] ASC);


