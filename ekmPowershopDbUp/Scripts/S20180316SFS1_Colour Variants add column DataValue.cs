using DbUp.Engine;
using System;
using System.Data;

namespace ekmPowershopDbUp.Scripts
{
    public class S20180316SFS1_ColourVariantsAddColumnDataValue : IScript
    {
        public string ProvideScript(Func<IDbCommand> commandFactory)
        {
            var command = commandFactory();

            //COLUMN EXISTS?
            bool exists = false;
            string tableName = "tbl_ProductOptionsItems";
            string columnName = "DataValue";

            command.CommandText = "PRAGMA TABLE_INFO("+ tableName +")";

            using (var reader = command.ExecuteReader())
            {
                int nameIndex = reader.GetOrdinal("Name");
                while (reader.Read())
                {
                    if (reader.GetString(nameIndex).Equals(columnName)) //CHECK FOR COLUMN NAME
                    {
                        exists = true;
                    }
                }
            }

            //UPDATE SCRIPT           
            if (!exists)
            {
                command.CommandText = "ALTER TABLE [tbl_ProductOptionsItems] ADD COLUMN [DataValue] TEXT";
                command.ExecuteNonQuery();
            }

            return "";
        }
    }
}
