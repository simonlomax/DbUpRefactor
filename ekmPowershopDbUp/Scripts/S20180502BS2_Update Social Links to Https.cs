﻿using DbUp.Engine;
using System;
using System.Data;

namespace ekmPowershopDbUp.Scripts
{
    class S20180502BS2_UpdateSocialLinkstoHttps : IScript
    {
        public string ProvideScript(Func<IDbCommand> commandFactory)
        {
            var command = commandFactory();

            System.Collections.Generic.List<string> queryList = new System.Collections.Generic.List<string>();

            command.CommandText = string.Format("SELECT [ElementContent], [ElementReference] FROM [tbl_Elements] WHERE [ElementReference] LIKE '_EKM_SOCIAL_WIDGET_COLLECTION_%' AND ElementContent LIKE '%http:%'");
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    string ElementContent = reader.GetString(0);
                    string ElementReference = reader.GetString(1);

                    ElementContent = ElementContent.Replace("http://", "https://");

                    queryList.Add(string.Format("UPDATE tbl_Elements SET ElementContent='{0}' WHERE ElementReference='{1}'", ElementContent.Replace("'", "''"), ElementReference));
                }
            }


            foreach (string query in queryList)
	        {
                command.CommandText = query;
                command.ExecuteNonQuery();		 
	        }

            return "";
        }
    }
}
