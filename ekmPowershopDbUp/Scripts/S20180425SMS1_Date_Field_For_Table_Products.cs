﻿using DbUp.Engine;
using System;
using System.Data;

namespace ekmPowershopDbUp.Scripts
{
    class S20180425SMS1_Date_Field_For_Table_Products : IScript
    {
        public string ProvideScript(Func<IDbCommand> commandFactory)
        {
            var command = commandFactory();

            var isExist = false;
            var tableName = "tbl_products";
            var columnName = "DateCreated";

            command.CommandText = string.Format("PRAGMA TABLE_INFO({0})", tableName);

            using (var reader = command.ExecuteReader())
            {
                var nameIndex = reader.GetOrdinal("name");
                while (reader.Read())
                {
                    if (reader.GetString(nameIndex).Equals(columnName)) isExist = true;
                }
            }

            if (!isExist)
            {
                command.CommandText = string.Format("ALTER TABLE [{0}] ADD COLUMN [{1}] DATETIME", tableName, columnName);
                command.ExecuteNonQuery();
            }

            command.CommandText = @"
                CREATE TRIGGER IF NOT EXISTS tbl_products_insert_date_created
                AFTER INSERT
                ON tbl_products
                    BEGIN
                        UPDATE tbl_products
                        SET DateCreated = CURRENT_TIMESTAMP
                        WHERE tbl_products.id = NEW.id;
                    END 
                ";
            command.ExecuteNonQuery();

            return "";
        }
    }
}
