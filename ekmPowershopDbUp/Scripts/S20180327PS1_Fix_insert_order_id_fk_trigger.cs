﻿using DbUp.Engine;
using System;
using System.Data;

namespace ekmPowershopDbUp.Scripts
{
    public class S20180327PS1_FixInsertOrderIdFkTrigger : IScript
    {
        public string ProvideScript(Func<IDbCommand> commandFactory)
        {
            var command = commandFactory();

            //COLUMN EXISTS?
            bool exists = false;
            string tableName = "tbl_orderitems";
            string columnName = "order_id";

            command.CommandText = "PRAGMA TABLE_INFO(" + tableName + ")";

            using (var reader = command.ExecuteReader())
            {
                int nameIndex = reader.GetOrdinal("Name");
                while (reader.Read())
                {
                    if (reader.GetString(nameIndex).Equals(columnName)) //CHECK FOR COLUMN NAME
                    {
                        exists = true;
                    }
                }
            }

            //UPDATE SCRIPT           
            if (!exists)
            {
                command.CommandText = "ALTER TABLE tbl_orderitems ADD order_id integer;";
                command.ExecuteNonQuery();
            }

            command.CommandText = "UPDATE tbl_orderitems SET order_id = (select id from tbl_orders where tbl_orders.order_number = tbl_orderitems.order_number);";
            command.ExecuteNonQuery();

            command.CommandText = "CREATE TRIGGER IF NOT EXISTS insert_order_id_fk " +
                                        "AFTER INSERT " +
                                        "ON tbl_orderitems " +
                                    "BEGIN " +
                                        "UPDATE tbl_orderitems " +
                                        "SET order_id = (select id from tbl_orders where tbl_orders.order_number = NEW.order_number) " +
                                        "WHERE tbl_orderitems.id = NEW.id; " +
                                    "END";
            command.ExecuteNonQuery();

            return "";
        }
    }
}
